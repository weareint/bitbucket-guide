
![2018-04-06_10-02-23.png](https://bitbucket.org/repo/ypnyLeB/images/3507166198-2018-04-06_10-02-23.png)
Start with creating a new repository on your bitbucket account for the project you would like to upload.


## Initialize
Open the terminal and `cd` (change directory) to the  project direcory on your computer.
```
cd <drag and drop project folder>
```
  
Init your repository on your machine. [This step requires to have git installed](https://git-scm.com/download/mac).
```
git init
```
  
Now the command line should response with something like this.
```
Initialized empty Git repository in <project folder>/.git
```
  
Tell `git` your remote repository and pull it. You can find the URL by clicking the `Clone` button on Bitbucket.
```
git remote add origin <repository url>
git pull origin master 
```
  
### Output
```
remote: Counting objects: 3, done.
remote: Compressing objects: 100% (2/2), done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
From https://bitbucket.org/<account>/<repository>
 * branch            master     -> FETCH_HEAD
 * [new branch]      master     -> origin/master
```
  
## Create a `.gitignore`

To exclude some files from syncing, create a file called `.gitignore` in the root directory.
```
# Node:

.node_modules
node_modules
*/node_modules
/node_modules/*

# OSX Files:

.DS_Store
Thumbs.db
*.swp
*.swo
*.lnk
*~
```


## Create a commit

Now you can create your first commit. Visual Studio Code provides a neat visual interface for that.

![2018-04-06_10-18-10.png](https://bitbucket.org/repo/ypnyLeB/images/3646902066-2018-04-06_10-18-10.png)
  
## Push your commit
```
git push --all
```

Output:
```
Counting objects: 15, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (15/15), done.
Writing objects: 100% (15/15), 910.79 KiB | 10.97 MiB/s, done.
Total 15 (delta 0), reused 0 (delta 0)
To https://bitbucket.org/<account>/<repository>
   0974902..2586e0c  master -> master
```

You're done.
![2018-04-06_10-22-09.png](https://bitbucket.org/repo/ypnyLeB/images/2203850526-2018-04-06_10-22-09.png)

